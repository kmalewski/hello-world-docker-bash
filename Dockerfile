FROM debian:testing-slim
LABEL Name=hello-world-docker-bash Version=0.0.1
COPY scripts /scripts/
RUN chmod oga+rx /scripts/*
WORKDIR /scripts
CMD ["./hello-world.sh"]

